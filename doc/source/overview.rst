.. index:: Overview

Overview
********
Modernize your VASP usage. No more collections of obscure bash scripts, no `sed`/`awk`-one liners, and no more wrestling with SLURM/Torque.

:program:`storq` consists of an advanced `ASE <https://wiki.fysik.dtu.dk/ase>`_
calculator for `VASP <http://vasp.at/>`_ and a command line tool that helps
with configuration and provides utility functions.

The goal of the calculator is to implement some heavy work-flow optimizations
that greatly reduce the overhead of your day-to-day VASP calculations
and provides a more customizable and modular approach compared to your typical
collection of bash scripts and "`sed`/`awk` one-liners".

The project started out as a fork of `John Kitchin's alternative ASE Vasp
calculator <https://github.com/jkitchin/vasp>`_.

Selected features
~~~~~~~~~~~~~~~~~
* VASP input files are automatically generated for you based on the calculator settings, no more POTCAR sorting, copying the wrong files from an old project or incorrectly sorted INCARs!
* Automatic submission of jobs on cluster, works out of the box for SLURM and can be configured to work with an arbitrary resource manager.
* Easy installation through `pip` automated configuration for cluster environments.
* Command-line tool with features such as calculation diagnostics and easy access to a cookbook of scripts with best practices and example calculations.
* Extensive list of post-processing methods.
* Different levels of convergence checks and restart options.
* Succesive unit cell relaxations via the `relax` method.
* A large set of qualtiy of life tools for archiving, backup, database management and more.
* Validation of the calculator keywords, including `INCAR` tags.

Workflow
========
The workflow optimizations of `Storq` come from new functionality built on top of the structure generation capabilities, code interfaces and post-porcessing tools already provided by ASE. By introducing configurable, automatic submission of jobs on HPC enviroments the workflow is linearized and the user never has to leave the comforting embrace of Python, as illustrated in the graphs below.
