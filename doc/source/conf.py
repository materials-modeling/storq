#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys
import re
import sphinx_rtd_theme

sys.path.insert(0, os.path.abspath("../../storq"))

extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.todo",
    "sphinx.ext.coverage",
    "sphinx.ext.mathjax",
    "sphinx.ext.ifconfig",
    "sphinx.ext.viewcode",
    "sphinx.ext.autosummary",
    "sphinx.ext.napoleon",
    "sphinx.ext.graphviz",
    "sphinx_sitemap",
]

graphviz_output_format = "svg"
templates_path = ["_templates"]
source_suffix = ".rst"
master_doc = "index"
pygments_style = "sphinx"
todo_include_todos = True


# Collect basic information from main module
with open("../../storq/__init__.py") as fd:
    lines = "\n".join(fd.readlines())
version = re.search("__version__ = '(.*)'", lines).group(1)
release = ""
copyright = re.search("__copyright__ = '(.*)'", lines).group(1)
project = re.search("__project__ = '(.*)'", lines).group(1)
author = re.search("__maintainer__ = '(.*)'", lines).group(1)

site_url = "https://storq.materialsmodeling.org/"
html_logo = "_static/logo.png"
html_favicon = "_static/logo.ico"
html_theme = "sphinx_rtd_theme"
html_static_path = ["_static"]
html_context = {
    "software": [
        ("atomicrex", "https://atomicrex.org/", "interatomic potential construction"),
        (
            "dynasor",
            "https://dynasor.materialsmodeling.org/",
            "dynamical structure factors from MD",
        ),
        (
            "hiPhive",
            "https://hiphive.materialsmodeling.org/",
            "anharmonic force constant potentials",
        ),
        ("icet", "https://icet.materialsmodeling.org/", "cluster expansions"),
        ("libvdwxc", "https://libvdwxc.org/", "library for van-der-Waals functionals"),
        (
            "storq",
            "https://storq.materialsmodeling.org/",
            "high-throughput submission system",
        ),
        (
            "vcsgc-lammps",
            "https://vcsgc-lammps.materialsmodeling.org/",
            "Monte Carlo simulations with lammps",
        ),
    ]
}
htmlhelp_basename = "storqdoc"

# Options for LaTeX output
_PREAMBLE = r"""
\usepackage{amsmath,amssymb}
\renewcommand{\vec}[1]{\boldsymbol{#1}}
\DeclareMathOperator*{\argmin}{\arg\!\min}
\DeclareMathOperator{\argmin}{\arg\!\min}
"""

latex_elements = {"preamble": _PREAMBLE}
latex_documents = [
    (
        master_doc,
        "storq.tex",
        "storq Documentation",
        "The storq developer team",
        "manual",
    )
]


# Options for manual page output
man_pages = [(master_doc, "storq", "storq Documentation", [author], 1)]


# Options for Texinfo output
texinfo_documents = [
    (
        master_doc,
        "storq",
        "storq Documentation",
        author,
        "storq",
        "Higher order force constants for everyone",
        "Miscellaneous",
    )
]
