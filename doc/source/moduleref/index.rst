.. _moduleref:

User interface
**************

.. toctree::
   :maxdepth: 2

   core
   charge_density

   io
   support_functions

   resource_manager
   tools
