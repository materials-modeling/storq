.. index:: Tutorials

Tutorials
*********
In the following a practical introduction of `Storq` is provided and several use cases, ranging from basic to more advance are provided.

**Disclaimer: The following tutorials assume basic knowledge of Python and ASE**

Note that all scripts here have been written in such a fashion that they will run regardless of whether you have set ``mode = 'run'`` or ``mode = 'queue'`` in your ``.vasprc``. **Thus, if you are in queue mode on the cluster a small job will be submitted**.

Basics
~~~~~~
This example illustrates how to relax a water molecule using VASP's internal relaxation routines.

.. literalinclude:: ../../storq/cli/cookbook/basic/water.py

The first argument of the ``Vasp`` constructor is the run directory where all VASP IO files will be created.
The configuration (ASE Atoms-object) is passed as a keyword argument. (Note: The calculator is internally attached to the atomic configuration scuh that calling :func:`atoms.get_potential_energy` still works. The exchange-correlation functional is set via the special keyword argument ``xc`` where only the name of the functional is needed. Any other ``INCAR`` parameters will be set automatically. Options pertaining to job submission are set using the :func:`configure` method of the calculator.

When submitting jobs via a queuing system, the results are not immediately available to the script. The calculator handles this by simply returning ``None`` if the calculation has not finished. After the job is finished one can simply rerun the script. This causes the calculator to restart and read the results without performing another calculation, after which additional analyses can be carried out.
