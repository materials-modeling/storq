.. raw:: html

  <p>
  <a href="https://gitlab.com/materials-modeling/storq/commits/master"><img alt="pipeline status" src="https://gitlab.com/materials-modeling/storq/badges/master/pipeline.svg" /></a>
  </p>

:program:`storq` — High-throughput VASP calculations with ASE
*************************************************************

:program:`storq` is a tool for efficiently conducting large number of
(routine) calculations as needed e.g., to generate input for cluster
expansions or analyses of surface and defect thermodynamics.

:program:`storq` is written in Python invokes functionality from
:ref:`external libraries <installation>` including
`ASE <https://wiki.fysik.dtu.dk/ase>`_.

:program:`storq` and its development are hosted on `gitlab
<https://gitlab.com/materials-modeling/storq>`_. Bugs and feature
requests are ideally submitted via the `gitlab issue tracker
<https://gitlab.com/materials-modeling/storq/issues>`_. The development
team can also be reached by email via storq@materialsmodeling.org.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   overview
   installation
   tutorials
   moduleref/index
