from setuptools import setup

setup(
    name="storq",
    version="0.9",
    description="High-throughput VASP calculations with ASE",
    url="http://storq.materialsmodeling.com",
    author="Joakim Loefgren, John Kitchin",
    author_email="joalof@chalmers.se",
    license="LGPL",
    platforms=["linux"],
    packages=["storq"],
    scripts=["bin/runvasp.py", "bin/storq", "bin/relax.py", "bin/vasp_chkstatus"],
    long_description="""Python module for setting up, running and analysing VASP calculations.""",
    install_requires=["ase>=3.17.0"],
    python_requires=">=3.6.0"
)
