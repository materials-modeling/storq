# Base image
FROM ubuntu:17.10

# Install required packages
RUN apt-get update -qy \
 && apt-get upgrade -qy \
 && apt-get install -qy python3-pip

# Set up some Python3 packages via pip
RUN pip3 install --upgrade pip
RUN pip3 install --upgrade setuptools
RUN pip3 install flake8 \
                 sphinx \
                 sphinx-rtd-theme \
                 sphinxcontrib-bibtex \
                 sphinx_sitemap
