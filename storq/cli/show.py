import platform
import sys
import os
import inspect
import shutil
import subprocess
import storq.vasp.readers as readers
from storq.tools import read_configuration, find_runs, getstatusoutput


def check_stderr(directory, display=False):
    stderr = None
    contents = None
    list_dir = os.listdir(directory)
    for stderr_name in ["stdoe", "stderr", "stderr.txt"]:
        if stderr_name in list_dir:
            stderr = os.path.join(directory, stderr_name)
            with open(stderr, "r") as fp:
                contents = fp.read()
            if contents != "":
                if display:
                    choice = input("An application wrote to stderr, display? [Y/n]")
                    if choice in ["", "Y", "y"]:
                        print(contents)
            break
    return stderr, contents


class CLICommand:
    short_description = (
        "Diagnose Vasp calculations and (optionally) fix them (if possible)."
    )

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("dir", type=str, help="The directory which to show")
        parser.add_argument(
            "-d",
            "--diagnose",
            action="store_true",
            default=False,
            help="Perform a diagnostics test on dir",
        )
        parser.add_argument(
            "-r",
            "--recursive",
            action="store_true",
            default=False,
            help="Combine with another flag to show all calculations in a folder.",
        )
        parser.add_argument(
            "-c",
            "--convergence",
            action="store_true",
            default=False,
            help="Print convergence related information.",
        )
        parser.add_argument(
            "-i",
            "--info",
            action="store_true",
            default=False,
            help="Print general information.",
        )
        parser.add_argument(
            "-v",
            "--verbose",
            action="store_true",
            default=False,
            help="Truns on verbose options where available",
        )

    @staticmethod
    def run(args):
        from storq import Vasp
        from storq.vasp.doctor import Doctor

        cwd = os.getcwd()
        base_input = ["INCAR", "POSCAR", "POTCAR"]
        vasp_conf = os.path.join(os.environ["HOME"], ".config/storq/vasp.json")
        vasp_out = read_configuration(vasp_conf)["vasp_stdout"]
        editor = os.environ.get("EDITOR", "vi")

        # if no mode was specified, default to printing the calc
        if not any([args.diagnose, args.convergence, args.info]):
            args.info = True

        if args.recursive:

            target_dirs = find_runs(args.dir)
            target_dirs = [os.path.abspath(d) for d in target_dirs]
            converged = []
            diagnostics = []

            for d in target_dirs:
                dname = os.path.basename(d)
                if args.diagnose:
                    doctor = Doctor(d)
                    diag = doctor.diagnose(verbose=args.verbose)
                    converged.append("Completely converged" in diag)
                    diagnostics.append(diag)
                if args.convergence:
                    print("-------------------------------------------------------")
                    print(
                        "    CONVERGENCE OF [[ {} ]]:".format(
                            os.path.join(args.dir, dname)
                        )
                    )
                    print("-------------------------------------------------------")
                    getstatusoutput(["vasp_chkstatus", os.path.join(d, "OUTCAR")])
                if args.info:
                    calc = Vasp(d)
                    print(calc)

            if args.diagnose:
                print("----------------------------------------------------------")
                print("    SUMMARY OF VASP RUNS IN [[ {} ]]:".format(args.dir))
                print("----------------------------------------------------------")
                nconv = converged.count(True)
                ntot = len(target_dirs)
                print("{}/{} runs  are completely converged.".format(nconv, ntot))
                if nconv < ntot:
                    print("...")
                    print("Diagnostics for non-converged runs:")
                    print("-----------------------------------")
                    for i in range(len(target_dirs)):
                        if not converged[i]:
                            print(
                                "[[ {} ]]: {}".format(
                                    os.path.basename(target_dirs[i]), diagnostics[i]
                                )
                            )
                            print("...")
        else:
            d = os.path.abspath(args.dir)
            if args.diagnose:
                doctor = Doctor(d)
                diag = doctor.diagnose(verbose=True)
            if args.convergence:
                getstatusoutput(["vasp_chkstatus", os.path.join(args.dir, "OUTCAR")])
            if args.info:
                calc = Vasp(d)
                print(calc)
