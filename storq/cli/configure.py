from collections import Counter
import platform
import sys
import numpy as np
import os
import sys
import subprocess
import storq.tools as tools
import sys


class CLICommand:
    short_description = "Configuration options for storq"

    @staticmethod
    def add_arguments(parser):
        parser.add_argument(
            "-a",
            "--auto",
            action="store_true",
            help="Trigger automatic configuration of storq",
        )
        parser.add_argument(
            "-f",
            "--file",
            type=str,
            help="Edit a config file, can be either vasp or site",
        )

    @staticmethod
    def run(args):
        """ Configure storq for a cluster/local computer. """
        if args.file:
            storq_dir = os.path.join(os.environ["HOME"], ".config", "storq")
            file_path = os.path.join(storq_dir, "{}.json")
            editor = os.environ.get("EDITOR", "vim")
            subprocess.call([editor, file_path.format(args.file)])

        if args.auto:
            print("----------------------------------------------------")
            print("               WELCOME TO STORQ                     ")
            print("----------------------------------------------------")
            print(
                """\
                 OOOOOOOOOOOOO
             OOOOOOOOOOOOOOOOOOOOOOO
          OOOOOOOOOOOOOOOOOOOOOOOOOOOOO
        OOOMMMMMMMMMOOOOOOOOOOOOOO  HMMM
      OOOMMMMMMMMMMMMOOOOOOOO      MMMMMMM
     OOO.......-MMMMMMOO:       7MMMMMMMMMMM
    OOOOOOO........MMMMM       MMMMMMOOOOOOOO
   OOOOOOOOO;.......MM      MMMMMMHOOOOOOOOOOO
  OOOOOOOOOOOO.......      MMMMMMOOOOOOOOOOOOOO
  OOO     OOOOO.....      MMMMMMOOOOOOOOOOOOOOO
 OOO      OOOOO.....      MMMMMOOOOOOOOOOOOOOOOO
 O OOOOOO  !OO!....      7MMMMMOOOOOOOOOOOOOOOOO
 OOOOOOOOOO                  MMOOOOOOOOOOOOOOOOO
  OOOOOOOOOO-                       ?OOOOOOOOOO
  OOOOOOOOOOO;                 OOOOOOOOOOOOOOOO
   OOOOOOOOOOOOOO            OOOOOOOOOOOOOOOOO
    OOOOOOOOOOOOOOOOOOOOOOO    OOOOOOOOOOOOOO
     OOOOOOOOOOOOOOOOOOOOOOOO OO OOOOOOOOOOO
      OOOOOOOOOOOOOOOOOOOOOOOO OO OOOOOOOOO
        OOOOOOOOOOOOOOOOOOOOOOO OOOOOOOOO
          OOOOOOOOOOOOOOOOOOOOOO  OO  O
             OOOOOOOOOOOOOOOOOOOOOOO
                 OOOOOOOOOOOOOO"""
            )
            print("")
            print("----------------------------------------------------")
            print("Storq will now be autmatically configured")
            print("----------------------------------------------------")
            # make sure that we have a folder for the configs
            config_dir = os.path.join(os.getenv("HOME"), ".config")
            if not os.path.exists(config_dir):
                os.mkdir(config_dir)

            storq_dir = os.path.join(config_dir, "storq")
            if not os.path.exists(storq_dir):
                os.mkdir(storq_dir)

            # Handle the site configurations
            # read any existing versions of the config file
            site_file = os.path.join(storq_dir, "site.json")
            siteconf = {}
            print("Detecting site configurations...")
            if os.path.exists(site_file):
                print(
                    (
                        "...existing site.json found!\n"
                        "...missing configs will be appended"
                    )
                )
                siteconf.update(tools.read_configuration(site_file))

            # determine the resource manager
            try:
                retcode, out, err = tools.getstatusoutput(
                    "sinfo --version".split(), stdout=subprocess.PIPE
                )
                siteconf["resource_manager"] = "slurm"
                print("...detected SLURM resource mangager")
            except OSError:
                retcode, out, err = tools.getstatusoutput(
                    'qmgr -c "p s" | grep pbs_ver',
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT,
                    shell=True,
                )
                if retcode == 0:
                    siteconf["resource_manager"] = "torque"
                    print("...detected Torque resource mangager...")
                else:
                    print(
                        (
                            "\nNo cluster resource manager detected!\n"
                            "...This is OK if you are not running vasp on a supercomputing resource"
                        )
                    )
                    siteconf["resource_manager"] = None

            resource_manager = siteconf["resource_manager"]

            # determine some cluster parameters
            if resource_manager == "slurm":
                info_string = subprocess.check_output("sinfo -N -o %z", shell=True)
                info_list = info_string.decode().split()[1:]
                node_info = {}
                node_info["sockets"] = Counter(
                    [n.split(":")[0].strip("+") for n in info_list]
                )
                node_info["cores_per_socket"] = Counter(
                    [n.split(":")[1].strip("+") for n in info_list]
                )
                node_info["threads_per_core"] = Counter(
                    [n.split(":")[2].strip("+") for n in info_list]
                )
                sockets = int(node_info["sockets"].most_common(1)[0][0])
                cores_per_socket = int(
                    node_info["cores_per_socket"].most_common(1)[0][0]
                )
                threads_per_core = int(
                    node_info["threads_per_core"].most_common(1)[0][0]
                )
                siteconf["cores_per_node"] = sockets * cores_per_socket
                siteconf["threads_per_core"] = threads_per_core

            # submission options
            siteconf["submit"] = {}
            submit = siteconf["submit"]
            if resource_manager == "slurm":
                submit["cmd"] = "sbatch"
                submit["workdir"] = "--chdir={}"
                submit["jobname"] = "--job-name={}"
                submit["account"] = "--account={}"
                submit["walltime"] = "--time={}"
                submit["nodes"] = "--nodes={}"
                submit["cores"] = "-n {}"
                submit["stdout"] = "-o {}"
                submit["stderr"] = "-e {}"
            elif resource_manager == "torque":  # WIP
                submit["cmd"] = "qsub"
                submit["workdir"] = "-d {}"
                submit["job_name"] = "-N {}"
                submit["account"] = "-A {}"
                submit["walltime"] = "l -walltime={}"
                submit["nodes"] = "l nodes={}"

            # job cancellation options
            siteconf["cancel"] = {}
            cancel = siteconf["cancel"]
            if resource_manager == "slurm":
                cancel["cmd"] = "scancel"
                cancel["user"] = "-u {}"
                cancel["name"] = "-n {}"

            # queue info options
            siteconf["queue"] = {}
            queue = siteconf["queue"]
            if resource_manager == "slurm":
                queue["cmd"] = "squeue"
                queue["jobid"] = "-j{}"
                queue["name"] = "-n {}"
                queue["user"] = "-u {}"

            # environmental variables
            siteconf["env"] = {}
            env = siteconf["env"]
            if resource_manager == "slurm":
                env["submitdir"] = "SLURM_SUBMIT_DIR"
                env["nodelist"] = "SLURM_JOB_NODELIST"
                env["tasks_per_node"] = "SLURM_TASKS_PER_NODE"
                env["jobid"] = "SLURM_JOB_ID"
            elif resource_manager == "torque":
                env["submitdir"] = "PBS_O_WORKDIR"
                env["nodefile"] = "PBS_NODEFILE"
                env["tasks_per_node"] = "SLURM_TASKS_PER_NODE"

            # write the config file
            # fname = os.path.join(config_dir, 'site.json')
            if resource_manager:
                tools.write_configuration(site_file, siteconf)
            else:
                tools.write_configuration(site_file, {"resource_manager": None})
            print("\nSuccessfully wrote config file to ~/.config/storq/site.json")

            # Handle the VASP configurations
            conf = {
                "vasp_mode": "run",  # either 'queue' or 'run'
                "mpi_command": None,  # command to run mpi applications
                "mpi_options": None,  # string containing options passed to mpi command
                "mpi_count_cores": False,  # automatic counting of num cores passed to mpi command
                "user": None,  # your user on supercomputing resource
                "batch_options": None,  # string containing options passed to submit command
                "batch_account": None,  # allocation on cluster, e.g., 'snic201x-x-xxx'
                "batch_walltime": None,  # batch_walltime for submitted jobs
                "batch_nodes": None,  # number of batch_nodes to run on
                "batch_cores": None,  # total number of cores to run on
                "batch_stdout": "stdout",  # where slurm redirects stdout (does not apply to VASP)
                "batch_stderr": "stderr",  # where slurm redirects stdout (does not apply to VASP)
                "vasp_convergence": "strict",  # basic or strict
                "vasp_restart_on": "convergence",  # basic, convergence or None
                "vasp_stdout": "vasp.out",  # save what vasp prints to vasp_stdout in a separate file
                "vasp_validate": True,  # check if calculator parameters are valid
                "vasp_executable_std": None,  # path to vasp vasp_executable_std
                "vasp_executable_gam": None,
                "vasp_executable_ncl": None,
                "vasp_potentials": None,  # path to folder where POTCARs are stored under folders potpaw_XXX
                "vasp_vdw_kernel": None,  # path to vdW kernel for automatic linking
            }

            print("\nDetecting vasp configurations...")
            flag_fatal = False
            vasp_file = os.path.join(storq_dir, "vasp.json")
            if os.path.exists(vasp_file):
                print(
                    (
                        "...existing vasp.json found\n"
                        "...missing configs will be appended"
                    )
                )
                conf.update(tools.read_configuration(vasp_file))

            # determine the mode
            if resource_manager:
                conf["vasp_mode"] = "queue"

            # determine the user name
            conf["user"] = os.environ.get("USER", None)

            # look for vasp binaries, first we look in PATH
            sys_path = os.environ.get("PATH").split(":")
            new_bins = ["vasp_std", "vasp_gam", "vasp_ncl"]  # VASP >= 5.4
            new_bins = new_bins + [b.replace("_", "-") for b in new_bins]
            old_bins = ["vasp_half", "vasp_gamma", "vasp_full"]  # VASP <= 5.3
            old_bins = old_bins + [b.replace("_", "-") for b in old_bins]
            dir_candidates = [
                p
                for p in sys_path
                if any(vasp in p for vasp in ["VASP", "Vasp", "vasp"])
            ]

            # first look for VASP >= 5.4 bins
            for vbin in new_bins:
                matches = [vbin in os.listdir(d) for d in dir_candidates]
                if any(matches):
                    bin_dir = dir_candidates[matches.index(True)]
                    tmp = vbin.replace("-", "_")
                    key = "vasp_executable_{}".format(tmp.split("_")[-1])
                    conf[key] = os.path.join(bin_dir, vbin)
                    print("...found {} executable".format(vbin))

            # If standard VASP >= 5.4 was not found look for <= 5.3
            if not conf.get("vasp_std", None):
                for vbin, key in zip(old_bins, new_bins):
                    matches = [vbin in os.listdir(d) for d in dir_candidates]
                    if any(matches):
                        bin_dir = dir_candidates[matches.index(True)]
                        tmp = vbin.replace("-", "_")
                        key = "vasp_executable_{}".format(tmp.split("_")[-1])
                        conf[key] = os.path.join(bin_dir, vbin)
                        print("...found {} executable".format(vbin))

            # if not in PATH, look for STANDARD VASP with find commands
            if not conf.get("vasp_executable_std", None):
                vasp_std = ""
                # regular timed find
                for vbin in ["vasp_std", "vasp-std", "vasp-half", "vasp_half"]:
                    vasp_std = tools.timed_find_file(vbin)
                    if vasp_std != "":
                        break
                # timed find and follow symlinks - slower
                for vbin in ["vasp_std", "vasp-std", "vasp-half", "vasp_half"]:
                    if vasp_std != "":
                        break
                    vasp_std = tools.timed_find_file(vbin, follow_symlinks=True)
                # by now we have either found it or given up
                if vasp_std != "":
                    conf["vasp_executable_std"] = vasp_std
                    print("...found vasp_std executable:     {}".format(vasp_std))
                else:
                    print(
                        (
                            "FATAL: could not find the vasp standard binary! (vasp_std), "
                            "update the config file manually with the path."
                        )
                    )
                    flag_fatal = True

            # look for GAMMA-ONLY version of VASP (this is optional)
            if not conf.get("vasp_executable_gam", None):
                vasp_gam = ""
                # regular timed find
                for vbin in ["vasp_gam", "vasp-gam", "vasp-gamma", "vasp_gamma"]:
                    vasp_gam = tools.timed_find_file(vbin)
                    if vasp_gam != "":
                        break
                # timed find and follow symlinks - slower
                for vbin in ["vasp_gam", "vasp-gam", "vasp-gamma", "vasp_gamma"]:
                    vasp_gam = tools.timed_find_file(vbin, follow_symlinks=True)
                    if vasp_gam != "":
                        break
                # by now we have either found it or given up
                if vasp_gam != "":
                    conf["vasp_executable_gam"] = vasp_gam
                    print("...found vasp_gam executable:     {}".format(vasp_gam))
                else:
                    print(
                        (
                            "\n WARNING: Could not find the vasp special binary vasp_gam,\n"
                            "update the config file manually with the path\n"
                            "if you want to use it with storq."
                        )
                    )

            # look for NON-COLLINEAR VASP, this is optional
            if not conf.get("vasp_executable_ncl", None):
                vasp_ncl = ""
                # regular timed find
                for vbin in ["vasp_ncl", "vasp-ncl", "vasp-full", "vasp_full"]:
                    vasp_ncl = tools.timed_find_file(vbin)
                    if vasp_ncl != "":
                        break
                # timed find and follow symlinks - slower
                for vbin in ["vasp_ncl", "vasp-ncl", "vasp-full", "vasp_full"]:
                    vasp_ncl = tools.timed_find_file(vbin, follow_symlinks=True)
                    if vasp_ncl != "":
                        break
                # by now we have either found it or given up
                if vasp_ncl != "":
                    conf["vasp_executable_ncl"] = vasp_ncl
                    print("...found vasp_gam executable:     {}".format(vasp_ncl))
                else:
                    print(
                        (
                            "WARNING: Could not find the vasp special binary vasp_ncl, "
                            "update the config file manually with the path "
                            "if you want to use it with storq."
                        )
                    )

            # look for vasp potentials
            dir_candidates = ["potpaw_PBE", "paw_PBE", "PBE"]
            for d in dir_candidates:
                potentials = tools.timed_find_file(d, name_type="d")
                if potentials != "":  # verify folder
                    if os.path.isfile(os.path.join(potentials, "README.UPDATES")):
                        break
            else:  # if no break try to follow symlinks
                for d in dir_candidates:
                    potentials = tools.timed_find_file(
                        "potpaw_PBE", follow_symlinks=True, name_type="d"
                    )
                    if potentials != "":  # verify folder
                        if os.path.isfile(os.path.join(potentials, "README.UPDATES")):
                            break
            if potentials != "":
                potentials = os.path.dirname(potentials)
                conf["vasp_potentials"] = potentials
                print("...found POTCARs:                 {}".format(potentials))
            else:
                print(
                    (
                        "FATAL: could not find the vasp pseudopotentials! "
                        "You need a folder containing subfolders potpaw_ABC, where ABC=PBE,LDA..., "
                        "update the config file manually with the path."
                    )
                )
                flag_fatal = True

            # look for vdw_kernel, there are typically many copies
            # take the one which is not in a VASP calculation directory
            vdw_kernel_list = tools.timed_find_file("vdw_kernel.bindat", first=False)
            if vdw_kernel_list == "":
                vdw_kernel_list = tools.timed_find_file(
                    "vdw_kernel.bindat", first=False, follow_symlinks=True
                )
            vdw_kernel = None
            if vdw_kernel_list != "":
                vdw_kernel_list = vdw_kernel_list.split()
                for i, vdw_kernel_path in enumerate(vdw_kernel_list):
                    d = os.path.dirname(vdw_kernel_path)
                    if not os.path.exists(os.path.join(d, "POSCAR")):
                        vdw_kernel = vdw_kernel_list[i]
                        break
            if vdw_kernel is None:
                print(
                    (
                        "\nWARNING: Could not find the vdw_kernel.bindat file, update the config file\n"
                        "manually with the path if you want to use the vdW-DF functionals\n"
                        "(Ignore if kernel is hard-coded)"
                    )
                )
            else:
                conf["vasp_vdw_kernel"] = vdw_kernel
                print("...found vdw_kernel.bindat")

            # try to determine mpi command
            for mpicmd in ["mpirun", "aprun", "mpiexec"]:
                retcode, out, err = tools.getstatusoutput(
                    "which {}".format(mpicmd).split(), stdout=subprocess.PIPE
                )
                if retcode == 0:
                    print("\nUsing mpi command:     {}".format(mpicmd))
                    conf["mpi_command"] = mpicmd
                    break
            else:
                print("FATAL: could not determine mpi run-command.")
                flag_fatal = True

            # Enable automatic counting of the number of cores by looking at slurm environmental
            # variables. Not needed if the mpi runner already does it for you.
            if (
                mpicmd in ["aprun", "mpiexec"]
                and siteconf["resource_manager"] is not None
            ):
                conf["mpi_count_cores"] = True

            tools.write_configuration(vasp_file, conf)
            if flag_fatal:
                print("\nWrote config file to ~/.config/storq/vasp.json")
                print("------------------------------------------")
                print(
                    (
                        "FATAL automatic configuration status: "
                        "one or more settings need to be set manually!"
                    )
                )
                print(
                    "To make the changes open config file with: storq configure -f vasp"
                )
            else:
                print("\nWrote config file to ~/.config/storq/vasp.json")
                print("\nSUCCESSFUL automatic configuration")
                print(
                    "\nREMEMBER TO SET REMAINING OPTIONS IN: ~/.config/storq/vasp.json"
                )
