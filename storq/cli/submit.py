import argparse
import platform
import sys
import os
import inspect
import shutil
import subprocess
import storq.vasp.readers as readers
from storq.tools import read_configuration, find_runs, getstatusoutput
from storq.tools import read_siteconf, read_batchconf


class CLICommand:
    short_description = "Submit a batch job."

    @staticmethod
    def add_arguments(parser):
        parser.add_argument("script", type=str, help="The python script to submit")
        parser.add_argument(
            "options",
            nargs=argparse.REMAINDER,
            help="Optional args passed on to the batch submit command.",
        )

    @staticmethod
    def run(args):

        site = read_siteconf()
        conf_on_file = read_batchconf()

        if site["resource_manager"] != "slurm":
            raise NotImplementedError(
                "Quick submission is only available for SLURM at the moment."
            )

        batch_script = "#!/bin/bash\n"
        prerun = conf.get("batch_prerun", None)
        if prerun:
            batch_script += prerun + "\n"
        batch_script += "{0} {1}".format("python", args.script)
        jobname = os.path.basename(args.script.split(".")[0])
        directory = os.getcwd()

        # Build a list of options to pass to sbatch
        cmdlist = ["{0}".format(site["submit"]["cmd"])]
        cmdlist.append(site["submit"]["workdir"].format(directory))
        cmdlist.append(site["submit"]["jobname"].format(jobname))

        # Only take account from on-file confs if it was not passed.
        if "-A" not in args.options and "--account" not in args.options:
            cmdlist.append(
                site["submit"]["account"].format(conf_on_file["batch_account"])
            )

        # Only take stdout/err filenames from on-file confs if they were not passed.
        cmdshort = ""
        if "-o" not in args.options and "--output" not in args.options:
            stdout = conf_on_file.get("batch_stdout", "stdout")
            cmdshort += " " + site["submit"]["stdout"].format(stdout)
        if "-e" not in args.options and "--error" not in args.options:
            stderr = conf_on_file.get("batch_stderr", "stderr")
            cmdshort += " " + site["submit"]["stderr"].format(stderr)

        # add short options to cmdlist
        cmdlist += cmdshort.split()

        # add any remaining options
        cmdlist += args.options

        # This is the block that acutally submits the job.
        p = subprocess.Popen(
            cmdlist,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        out, err = p.communicate(batch_script.encode())
        if err != b"":
            raise Exception(
                "something went wrong in sbatch:\n\n{0}".format(err.decode())
            )

        # Write jobid and finish, note that the state changes.
        jobid = out.strip().split()[3].decode()
        print("Submitted batch job {0}: {1}".format(jobid, jobname))
