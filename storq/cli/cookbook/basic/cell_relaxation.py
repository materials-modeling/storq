from ase.build import bulk
from storq import Vasp

atoms = bulk("Au", a=4.09)

calc = Vasp(
    "cell_relaxation",
    atoms=atoms,
    xc="pbe",
    encut=300,
    isif=3,
    ibrion=1,
    nsw=20,
    ediffg=-0.005,
    ediff=1e-6,
    ismear=1,
    sigma=0.1,
    kpts={"size": [14, 14, 14], "gamma": False},
)

calc.configure(walltime="00:10:00", cores=1)

# run cell relaxation
calc.relax(max_runs=10, backup="minimal")

# analyze results if calculation is finished
if calc.ready():
    print("Lattice constant {:.4f}".format(atoms.cell[0][1] * 2))
