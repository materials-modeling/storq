from ase import Atom, Atoms
from storq import Vasp
import matplotlib.pyplot as plt
import numpy as np

# Create CO molecules with different bond lengths
bond_lengths = np.linspace(1.05, 1.25, 10)
molecules = [
    Atoms([Atom("C", [0, 0, 0]), Atom("O", [b, 0, 0])], cell=(8, 8, 8))
    for b in bond_lengths
]

# Loop over configs and calculate energy
# Note: gamma only is the default so we actually don't need to set it here.
energies = []
for b, mol in zip(bond_lengths, molecules):
    calc = Vasp(
        "bond_length/CO_bond_{:.2f}".format(b),  # rundirs
        atoms=mol,  # atoms passed as kwarg
        xc="pbe",
        encut=300,
        kpts={"size": [1, 1, 1], "gamma": True},
        isif=1,
        ismear=0,
        sigma=0.05,
    )

    # job submission options
    calc.configure(walltime="00:10:00", cores=4)

    # get the energies (this triggers the calculations)
    energies.append(calc.get_potential_energy())

# If a job isn't finished, None is returned:
# we use this to exit if we are waiting for submitted jobs
Vasp.stop_if(None in energies)

# Analyze results
fig, ax = plt.subplots()
ax.plot(bond_lengths, energies)
ax.set_xlabel(r"Bond length ($\AA$)")
ax.set_ylabel(r"Energy (eV)")
ax.set_title("CO molecule")
plt.savefig("CO_bond_length.png")
