from ase.build import molecule
from storq import Vasp

# set up atomic configuration
atoms = molecule("H2O")
atoms.center(vacuum=4.0)

# set up calculator
calc = Vasp(
    "water",
    atoms=atoms,
    xc="pbe",
    encut=300,
    ibrion=1,
    nsw=20,
    isif=2,
    ismear=0,
    sigma=0.05,
)

# set submission options
calc.configure(walltime="00:05:00", cores=4)

# run vasp
calc.get_potential_energy()
