from ase.build import bulk
from storq import Vasp
from ase.units import GPa
from ase.eos import EquationOfState
import numpy as np

# bulk configurations with different volume
a0 = 4.16
scaling = np.linspace(0.95, 1.05, 10)
configs = [bulk("Au", "fcc", a=a0 * scale) for scale in scaling]

# generate calculators via list comprehension
calculators = [
    Vasp(
        "equation_of_state/Au_bulk_alatt_{:.2f}".format(a0 * scale),  # rundirs
        xc="pbe",
        encut=300.0,
        kpts={"size": [8, 8, 8], "gamma": False},
        ismear=1,
        sigma=0.1,
        atoms=conf,
    )
    for scale, conf in zip(scaling, configs)
]

# cluster job settings for all calculators
for calc in calculators:
    calc.configure(walltime="00:15:00", cores=1)

# run calculations
energies = [calc.get_potential_energy() for calc in calculators]
#  equivalently we could have written
# energies = [conf.get_potential_energy() for conf in configs]

# If a job isn't finished, None is returned:
# we use this to exit if we are waiting for submitted jobs
Vasp.stop_if(None in energies)

# analyze results
volumes = [conf.get_volume() for conf in configs]
eos = EquationOfState(volumes, energies)
v0, e0, B = eos.fit()

print("----------------------------")
print("Equation of state parameters")
print("----------------------------")
print("E0: {:2.6f} eV".format(e0))
print("V0: {:2.1f} A^3".format(v0))
print("B: {:2.1f} GPa".format(B / GPa))
eos.plot("Au_eos.png")
