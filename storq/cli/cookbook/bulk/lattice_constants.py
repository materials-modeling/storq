from ase.build import bulk
from storq import Vasp

# elements and guesses for lattice constants
lattice_info = {"Cu": 3.58, "Pd": 3.88, "Ag": 4.06, "Pt": 3.93, "Au": 4.09}

for elem, alat in lattice_info.items():
    atoms = bulk(elem, crystalstructure="fcc", a=alat, cubic=False)
    calc = Vasp(
        "lattice_constants/{}".format(elem),
        atoms=atoms,
        xc="cx",
        encut=450,
        kpts={"spacing": 0.2, "gamma": False},
        ibrion=1,
        nsw=20,
        isif=3,
        ediffg=-0.01,
        ediff=1e-6,
        algo="Normal",
        ismear=1,
        sigma=0.1,
        prec="Accurate",
        lreal=False,
        addgrid=True,
        lwave=False,
        lcharg=False,
        nsim=2,
        ncore=8,
    )

    calc.configure(walltime="00:30:00", cores=2)
    calc.relax(max_runs=10, backup="minimal")
