from ase.build import surface
from ase.db import connect
import itertools
from ase.io import read, write
from ase.visualize import view
import sys


def set_config(calc):
    """ Convenience function to set
    configs that depends on natoms etc. """
    settings = {}
    natoms = len(calc.atoms)
    settings["nodes"] = 1
    settings["walltime"] = "04:00:00"
    if natoms >= 10:
        settings["nodes"] = 2
    return settings


# specification of the structures and XC functional
element = "Au"
miller_index = 111
nlayers = range(4, 17)
vacuum = 10.0
xc = "cx"

energies = []
for nlay in nlayers:
    slab = fcc111("Au", a=4.09, cubic=False)
    slab.center(vacuum=vacuum, axis=2)
    name = "Au_111_nl{}_cx".format(nlay)
    calc = Vasp(
        "surfaces/{}".format(name),
        atoms=row.toatoms(),
        xc="cx",
        encut=400,
        kpts={"spacing": 0.2, "gamma": True, "surface": True},
        ibrion=1,
        nsw=40,
        isif=2,
        ediffg=-0.01,
        ediff=1e-6,
        algo="Normal",
        ismear=1,
        sigma=0.1,
        prec="Accurate",
        lreal=False,
        addgrid=True,
        lwave=True,
        lcharg=False,
        nsim=2,
        npar=2,
        kpar=2,
    )

    calc.configure(**set_config(calc))
    energies.append(calc.get_potential_energy())
