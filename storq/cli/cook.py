import platform
import sys
import os
import inspect
import shutil
from ase.utils import import_module
from ase.io.formats import filetype, all_formats, UnknownFileTypeError


def get_dir_contents(path, skip=None, display=True):
    """ Returns all the contents in the dir specified
    by path with options to skip either all files
    or all directories and print the result.
    """
    contents = []
    for item in os.listdir(path):
        path_item = os.path.join(path, item)
        if skip == "files":
            if os.path.isdir(path_item):
                contents.append(path_item)
        elif skip == "dirs":
            if not os.path.isdir(path_item):
                contents.append(path_item)
    contents = sorted(contents, key=str.lower)
    if display:
        for i, item in enumerate(contents):
            print("{}) {}".format(i + 1, os.path.basename(item)))
    return contents


class CLICommand:
    short_description = "Obtain a template for a commonly performed calculation"

    @staticmethod
    def add_arguments(parser):
        parser.add_argument(
            "-d",
            "--dir",
            type=str,
            help="the dirname of the recipe collection you want to browse",
        )
        parser.add_argument(
            "-f", "--file", type=str, help="the filename of the recipe you want to copy"
        )
        # parser.add_argument('-v', '--verbose', action='store_true')

    @staticmethod
    def run(args):
        path_base = os.path.join(os.path.split(__file__)[0], "cookbook")
        # list all availalbe recipe collections and let user choose
        if not args.dir:
            print("Available recipe categories")
            collections = get_dir_contents(path_base, skip="files", display=True)
            ichoice = int(input("Choose a category by typing its number: ")) - 1
            path_collection = collections[ichoice]
        else:
            path_collection = os.path.join(path_base, args.dir)

        # list all availalbe recip collections and let user choose
        if not args.file:
            print("Available recipes in category:")
            recipes = get_dir_contents(path_collection, skip="dirs", display=True)
            ichoice = int(input("Choose a recipe to copy by typing its number: ")) - 1
            path_recipe = recipes[ichoice]
        else:
            path_recipe = os.path.join(path_collection, path_recipe)

        shutil.copy(path_recipe, os.getcwd())
