import os
from pathlib import Path

import numpy as np

from storq.vasp.core import Vasp
import storq.vasp.readers as readers
from storq.tools import read_db
from storq.tools import read_configuration


class Doctor:
    """Class for the VASP calculators error checking capabilities.

    Adapted from Materials Genome Project Custodian.
    https://github.com/materialsproject/custodian
    """

    # dict containing all the aliases of some errors recognized by VASP
    # and the corresponding text that gets written to vasp_stdout.
    known_errors = {
        "tet": [
            "Tetrahedron method fails for NKPT<4",
            "Fatal error detecting k-mesh",
            "Fatal error: unable to match k-point",
            "Routine TETIRR needs special values",
        ],
        "inv_rot_mat": [
            "inverse of rotation matrix was not found (increase " "SYMPREC)"
        ],
        "brmix": ["BRMIX: very serious problems"],
        "subspacematrix": ["WARNING: Sub-Space-Matrix is not hermitian in " "DAV"],
        "tetirr": ["Routine TETIRR needs special values"],
        "incorrect_shift": ["Could not get correct shifts"],
        "real_optlay": ["REAL_OPTLAY: internal error", "REAL_OPT: internal ERROR"],
        "rspher": ["ERROR RSPHER"],
        "dentet": ["DENTET"],
        "too_few_bands": ["TOO FEW BANDS"],
        "triple_product": ["ERROR: the triple product of the basis vectors"],
        "rot_matrix": ["Found some non-integer element in rotation matrix"],
        "brions": ["BRIONS problems: POTIM should be increased"],
        "pricel": ["internal error in subroutine PRICEL"],
        "zpotrf": ["LAPACK: Routine ZPOTRF failed"],
        "amin": ["One of the lattice vectors is very long (>50 A), but AMIN"],
        "zbrent": ["ZBRENT: fatal internal in", "ZBRENT: fatal error in bracketing"],
        "pssyevx": ["ERROR in subspace rotation PSSYEVX"],
        "eddrmm": ["WARNING in EDDRMM: call to ZHEGV failed"],
        "edddav": ["Error EDDDAV: Call to ZHEGV failed"],
        "grad_not_orth": ["EDWAV: internal error, the gradient is not orthogonal"],
        "nicht_konv": ["ERROR: SBESSELITER : nicht konvergent"],
        "zheev": ["ERROR EDDIAG: Call to routine ZHEEV failed!"],
        "elf_kpar": ["ELF: KPAR>1 not implemented"],
        "elf_ncl": ["WARNING: ELF not implemented for non collinear case"],
        "rhosyg": ["RHOSYG internal error"],
        "posmap": ["POSMAP internal error: symmetry equivalent atom not found"],
        "lattyp": ["internal error in subroutine LATTYP"],
    }

    causes = {
        "tet": "The symmetry of the lattice may be incompatible with the k-point grid",
        "inv_rot_mat": (
            "The k-point mesh is possibly breaking the symmetry"
            "can happen for meshes with even number of kpts in combination with"
            "hexagonal or low-symmetry lattices"
        ),
        "brmix": (
            "First try to switch to IMIX=1, if the error persists then"
            "switch from gamma-centered to MKP or vice versa"
        ),
        "subspacematrix": (
            "Possibly due to an unreasonable input geometry"
            "see https://cms.mpi.univie.ac.at/vasp-forum/viewtopic.php?t=1192"
        ),
        "tetirr": (
            "Tetrahedron method k-point meshes must include the Gamma point"
            "for subdividing the BZ into tertahedra"
        ),
        "incorrect_shift": "Switch to Gamma-centered grid if using MKP.",
        "real_optlay": (
            "Possibly problems with space projection operators (due to POTCARs"
            "having very different cut-offs?)"
        ),
        "rspher": "Possibly related to problems with memory allocation for large systems",
        "nicht_konv": (
            "Due to a spherical bessel function that cannot be calculated"
            "with the required accuracy"
        ),
        "dentet": (
            "The fermi level of the system cannot be determined accurately enough"
            "by the tetrahedron method."
        ),
        "too_few_bands": (
            "More electronic bands need to be included."
            "Note that even values automatically chosen"
            "by VASP can yield this error."
        ),
        "triple_product": (
            "The unit cell is left-handed and VASP requires" "a right-handed cell."
        ),
        "rot_matrix": (
            "Related to symmetry and can pop up if convergence"
            "parameters are set too loosely"
        ),
        "brions": "Possibly due to a bad choice of the ionic minimization step length",
        "pricel": "See http://cms.mpi.univie.ac.at/vasp-forum/viewtopic.php?f=3&t=10201",
        "zpotrf": "Typically because binding distances in the input geometry are too short.",
        "amin": "Having a very long  cell vector can cause charge-sloshing.",
        "zbrent": "Malfunction of the ionic optimization algorithm associated with IBRION=2",
        "pssyevx": "Could be due to bad POSCAR, or over-parallelization for a small system",
        "eddrmm": "RMM-DIIS algorithm has failed in the SCF cycle",
        "edddav": "Davidson-algorithm has failed in the SCF cycle",
        "grad_not_orth": "Can occur for hybrid calculations making them hang.",
        "zheev": (
            "Due to failure of the diagonalization algorithm"
            "employed during the SCF cycle"
        ),
        "elf_kpar": "The ELF function is not implemented for k-point parallelization.",
        "elf_ncl": "The ELF function is not implemented for spin-polarized calculations.",
        "rhosyg": "Symmetry-related",
        "posmap": (
            "Symmetry-related (a symmetry-equivalent atom is not"
            "found where expected)"
        ),
        "lattyp": "Invalid POSCAR, typically with overlapping or identical atomic positions",
    }

    fixes = {
        "tet": "Set ISMEAR=0.",
        "inv_rot_mat": (
            "Decrease SYMPREC, i.e., use a smaller value than"
            "the default 1e-5 such as 1e-8"
        ),
        "brmix": (
            "First try to switch to IMIX=1, if the error persists then"
            "switch from gamma-centered to MKP or vice versa"
        ),
        "subspacematrix": (
            "Set LREAL=.FALSE., if the error persists then set PREC=Accurate"
        ),
        "tetirr": "Switch to Gamma-centered grid if using MKP.",
        "incorrect_shift": "Switch to Gamma-centered grid if using MKP.",
        "real_optlay": (
            "Set LREAL=.FALSE., if the cell is large try"
            "LREAL=.TRUE. first. Can also try sorting POSCAR so that"
            "highest ENMAX comes first."
        ),
        "rspher": ("Set LREAL=.FALSE., if the cell is large try" "LREAL=.TRUE. first"),
        "nicht_konv": (
            "Set LREAL=.FALSE., if the cell is large try" "LREAL=.TRUE. first"
        ),
        "dentet": "Set ISMEAR=0.",
        "too_few_bands": "Increase NBANDS by a suitable factor, e.g. 1.1",
        "triple_product": (
            "Change the handedness of the cell from left-handed" "to right-handed"
        ),
        "rot_matrix": "If MKP switch to gamma-centered, else set ISYM=0",
        "brions": "Increase POTIM, e.g, by increments of 0.1",
        "pricel": (
            "Decrease SYMPREC, i.e., use a smaller value than"
            "the default 1e-5 such as 1e-8 and set ISYM=0"
        ),
        "zpotrf": (
            "If on the first  ionic step, increase the volume of the"
            "unit cell, otherwise decrease POTIM by a factor of 0.5."
            "For static runs try setting ISYM=0"
        ),
        "amin": "Set AMIN=0.01.",
        "zbrent": "Restart calculation with IBRION=1.",
        "pssyevx": "Set ALGO=Normal",
        "eddrmm": (
            "Set ALGO=Normal, halve POTIM and delete" "WAVECAR/CHARGCAR if ICHARG<10"
        ),
        "edddav": "Set ALGO=All and delete CHGCAR if ICHARG<10",
        "grad_not_orth": "If hybrid calcultion, start from pre-converged WAVECAR",
        "zheev": "Set ALGO=Exact",
        "elf_kpar": "Set KPAR=1",
        "elf_ncl": "Switch to a non-spin polarized calculation or turn off the ELF calculation",
        "rhosyg": "Set SYMPREC=1e-4, if error persists set ISYM=0",
        "posmap": "Set SYMPREC=1e-6",
        "lattyp": "Make sure the atomic positions and binding distances are reasonable",
    }

    def __init__(self, directory):
        """The doctor is in.

        The Doctor instance knows the information contained
        in a storq database, if it exists.

        Parameters
        ----------
        directory : str
            The path (absolute or relative) the the calculation directory

        """
        self.directory = Path(directory).resolve()
        try:
            atoms, data = read_db(directory)
        except KeyError:
            atoms, data = None, None
        self.data = data
        self.atoms = atoms

    def check(self, target_errors="all", large_cell_limit=100, verbose=False):
        """Checks a calculation against a list of known errors.

        Given a directory containing a calculation the VASP std output
        will be checked against a list of known errors. All detected errors
        are then stored in an attribute so they can be fixed later.

        Parameters
        ----------
        target_errors : str or list
            Can be 'all' or a list of specific errors to look for
        large_cell_limit : int
            number of atoms defined to constitute a large cell, this will
            impact the correction of certain errors.
        verbose : bool
            Doctor becomes chatty

        Returns
        -------
        set
            All errors that were detected.

        """
        errors = set()
        if self.data is not None:
            vasp_stdout = self.data["conf"].get("vasp_stdout", None)
        if self.data is None or vasp_stdout is None:
            return errors

        # determine which errors to check for
        if target_errors == "all":
            target_errors = self.known_errors

        # check vasp vasp_stdout for error messages
        vasp_stdout = self.directory.joinpath(vasp_stdout)
        with open(vasp_stdout, "r") as fp:
            for line in fp:
                line_strip = line.strip()
                for err, msgs in self.known_errors.items():
                    if err in target_errors:
                        for msg in msgs:
                            if line_strip.find(msg) != -1:
                                # this checks if we are running charged
                                # computation (e.g., defects). If yes we don't
                                # want to log an error due to a change in
                                # e-density which leads to a BRMIX error
                                if err == "brmix" and self.parameters.get(
                                    "nelect", None
                                ):
                                    continue
                                errors.add(err)
                                if verbose:
                                    print("--------------------------------")
                                    print("DETECTED ERROR {}".format(err))
                                    print("--------------------------------")
        return errors

    def read_stderr(self, display=False):

        # first we need to determine which files to check for
        # std output, if there is a storq database we try to get
        # the names from there, otherwise go with the default
        if self.data is not None:
            batch_stdout = self.data["conf"].get("batch_stdout", None)
            batch_stderr = self.data["conf"].get("batch_stderr", None)
        else:
            batch_stdout = None
            batch_stderr = None
        if self.data and not batch_stdout:
            batch_stdout = "slurm-{}.out".format(self.data["jobid"])
        if not batch_stderr:
            batch_stderr = batch_stdout

        # look for a file with the right name
        stderr = None
        contents = None
        list_dir = list(self.directory.iterdir())
        for stderr_name in [batch_stderr, "stdoe", "stderr", "stderr.txt", "stdoe.txt"]:
            if stderr_name in list_dir:
                stderr = self.directory.joinpath(stderr_name)
                with open(stderr, "r") as fp:
                    contents = fp.read()
                if contents != "":
                    if display:
                        choice = input("An application wrote to stderr, display? [Y/n]")
                        if choice in ["", "Y", "y"]:
                            print(contents)
                break
        return stderr, contents

    def diagnose(self, verbose=True):
        """ Try to determine if anything is wrong with a calculation. """
        base_input = ["INCAR", "POSCAR", "POTCAR"]
        d = self.directory
        dname = "[" + d.name + "]"
        if verbose:
            print("Running diagnostics for {}:".format(dname))

        # does the current dir contain vasp input files?
        list_dir = [f.name for f in d.iterdir()]
        if not all([f in list_dir for f in base_input]):
            diag = "Some VASP input files found."
            if verbose:
                print("...{}".format(diag))
            return diag

        # check if VASP was ever run in the folder
        if not "OUTCAR" in list_dir:
            # Either calculation was never run or vasp was cancelled
            # before OUTCAR could be written
            # look for stderr files that can tell us more
            # msg = 'Missing OUTCAR.'
            if verbose:
                print("...Missing OUTCAR")

            stderr, _ = self.read_stderr(display=True)
            if stderr is None:
                diag = "A calculation was never or the OUTCAR has been removed."
                if verbose:
                    print("...found no external error logs")
                    print("Diagnosis: {}".format(diag))
                    print("--------------------------")
                return diag
        # vasp has been run
        else:
            # found an OUTCAR, check if VASP terminated OK
            outcar = d.joinpath("OUTCAR")
            valid = readers.read_outcar_validity(outcar)

            # no, something went wrong before VASP finished
            if not valid:
                if verbose:
                    print("...OUTCAR was only partially written")
                stderr, contents = self.read_stderr(display=False)
                if stderr:
                    if verbose:
                        print("...found an external error log")
                    if all([word in contents for word in ["CANCELLED", "TIME LIMIT"]]):
                        diag = "Job hit the walltime"
                        if verbose:
                            print("Diagnosis: {}".format(diag))
                            print("--------------------------")
                        return diag

                    elif all([word in contents for word in ["CANCELLED", "USER"]]):
                        diag = "Job was cancelled by the user"
                        if verbose:
                            print("Diagnosis: {}".format(diag))
                            print("--------------------------")
                        return diag
                    else:
                        choice = input("An application wrote to stderr, display? [Y/n]")
                        if choice in ["", "Y", "y"]:
                            print(contents)

                # now we check for internal vasp errors
                doctor = Doctor(d)
                errors = doctor.check()
                if len(errors) > 0:
                    if verbose:
                        print("...one or more VASP errors encountered!".format(dname))
                    for err in errors:
                        diag = "Found VASP error: {0}\nCauses: {1}\nFixes: {2}\n".format(
                            self.known_errors[err], self.causes[err], self.fixes[err]
                        )
                        if verbose:
                            print(diag)
                        return diag
                else:
                    diag = "Could not determine what went wrong."
                    if verbose:
                        print("Diagnosis: {}".format(diag))
                        print("--------------------------")
                    return diag

            # yes, everything good so far
            else:
                # check for convergence
                if verbose:
                    print("...found a valid OUTCAR")
                    print("...testing convergence")
                converged = readers.read_convergence(outcar)
                if all(converged):
                    diag = "Completely converged"
                    if verbose:
                        print("Diagnosis for {}: completely converged".format(dname))
                        print("--------------------------")
                    return diag

                elif converged[0] and not converged[1]:
                    diag = (
                        "Ionic positions are not relaxed (electronic SCF cycle is OK)."
                    )
                    if verbose:
                        print("Diagnosis: {}".format(diag))
                    return diag
                elif converged[1] and not converged[0]:
                    diag = "Ionic positions are relaxed, but the electronic SCF cycle did not converge"
                    if verbose:
                        print("Diagnosis: {}".format(diag))
                        print("--------------------------")
                    return diag
                else:
                    diag = "Ionic positions are not relaxed and the electronic SCF cycle did not converge"
                    if verbose:
                        print("Diagnosis: {}".format(diag))
                        print("--------------------------")
                    return diag
