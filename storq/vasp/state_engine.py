class StateEngine:
    """ Determines action based on the state. """

    def __init__(self, calc, run_method="_run", *args, **kwargs):
        self.calc = calc
        self.run_method = run_method
        self.args = args
        self.kwargs = kwargs

    def __enter__(self):
        if self.calc.calculation_required():
            getattr(self.calc, self.run_method)(*self.args, **self.kwargs)
        else:
            if self.calc.state in [self.calc.RUNNING, self.calc.QUEUED]:
                return None
            if self.calc.state != self.calc.PARSED:
                self.calc.read_results()

    def __exit__(self, *args):
        pass
