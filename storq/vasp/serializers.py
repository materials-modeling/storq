import gzip
import os
import subprocess
import shutil
import tarfile
import warnings

import numpy as np

import storq.vasp.readers as readers
import storq.tools as tools


class Serializers:
    def todict(self, skip_default=True):
        """.

        Parameters
        ----------
        skip_default : bool

        Returns
        -------
        ...

        """
        defaults = self._default_parameters
        dct = {}
        for key, value in self.parameters.items():
            if hasattr(value, "todict"):
                value = value.todict()
            if skip_default:
                default = defaults.get(key, "_no_default_")
                if default != "_no_default_" and tools.is_equal(value, default):
                    continue
            dct[key] = value
        return dct

    def __str__(self):
        """Pretty VASP representation of a calculation."""
        s = ["\n", "Vasp calculation directory:"]
        s += ["---------------------------"]
        s += ["  [[{self.directory}]]"]
        atoms = self.atoms
        cell = atoms.get_cell()
        A, B, C = [i for i in cell]
        latt = list(map(np.linalg.norm, cell))
        a, b, c = latt
        alpha = np.arccos(np.dot(B / b, C / c)) * 180 / np.pi
        beta = np.arccos(np.dot(A / a, C / c)) * 180 / np.pi
        gamma = np.arccos(np.dot(A / a, B / b)) * 180 / np.pi

        s += ["\nSystem:"]
        s += ["-----------------"]
        s += tools.ascii_atoms(self.atoms)

        s += ["\nCell:"]
        s += ["    {:^8}{:^8}{:^8}{:>12}".format("x", "y", "z", "|v|")]
        for i, v in enumerate(cell):
            s += [
                "  v{0}{2:>8.3f}{3:>8.3f}{4:>8.3f}"
                "{1:>12.3f} Ang".format(i, latt[i], *v)
            ]
        s += [
            "\n  alpha, beta, gamma (deg):"
            "{:>6.1f}{:>6.1f}{:>6.1f}".format(alpha, beta, gamma)
        ]
        volume = atoms.get_volume()
        s += ["  Total volume:{:>25.3f} Ang^3".format(volume)]

        if self.state in [self.FINISHED, self.CONVERGED, self.UNCONVERGED]:
            self.read_results()

        stress = self.results.get("stress", None)
        if stress is not None:
            s += [
                "  Stress:{:>6}{:>7}{:>7}"
                "{:>7}{:>7}{:>7}".format("xx", "yy", "zz", "yz", "xz", "xy")
            ]
            s += [
                "{:>15.3f}{:7.3f}{:7.3f}" "{:7.3f}{:7.3f}{:7.3f} GPa\n".format(*stress)
            ]

        s += ["\nAtoms:"]
        s += [
            "  {:<4}{:<8}{:<3}{:^10}{:^10}{:^10}"
            "{:>14}".format("ID", "tag", "sym", "x", "y", "z", "rmsF (eV/A)")
        ]
        from ase.constraints import FixAtoms, FixScaled

        constraints = [[None, None, None] for atom in atoms]
        for constraint in atoms.constraints:
            if isinstance(constraint, FixAtoms):
                for i in constraint.index:
                    constraints[i] = [True, True, True]
            elif isinstance(constraint, FixScaled):
                constraints[constraint.a] = constraint.mask.tolist()

        forces = self.results.get("forces", None)
        if forces is not None:
            for i, atom in enumerate(atoms):
                rms_f = np.sum(forces[i] ** 2) ** 0.5

                s += [
                    "  {:<4}{:<8}{:3}{:9.3f}{}{:9.3f}{}{:9.3f}{}"
                    "{:>10.2f}".format(
                        i,
                        atom.tag,
                        atom.symbol,
                        atom.x,
                        "*" if constraints[i][0] is True else " ",
                        atom.y,
                        "*" if constraints[i][1] is True else " ",
                        atom.z,
                        "*" if constraints[i][1] is True else " ",
                        rms_f,
                    )
                ]

        energy = self.results.get("energy", np.nan)
        s += ["\n  Potential energy: {:.4f} eV".format(energy)]

        if self.state in [self.FINISHED, self.CONVERGED, self.UNCONVERGED]:
            if self.conf["vasp_convergence"] == "strict":
                s += ["  Converged: {} ".format(self.has_converged())]
            elif self.conf["vasp_convergence"] == "basic":
                s += ["  Finished: {} ".format(self.has_valid_outcar())]
            else:
                s += ["  CONVERGENCE CHECKS ARE DISABLED"]

        s += ["\nINPUT Parameters:"]
        s += ["-----------------"]
        for key, value in self.parameters.items():
            if type(value) != dict:
                s += ["  {0:<10} = {1}".format(key, value)]
            elif key == "kpts":
                s += ["  kpts       = {{"]
                for kpts_key, kpts_val in value.items():
                    if kpts_val is not None:
                        if kpts_key == "surface" and kpts_val is False:
                            pass
                        elif kpts_key == "shift" and np.isclose(np.sum(kpts_val), 0.0):
                            pass
                        else:
                            s += ["     {0:>17} : {1}".format(kpts_key, kpts_val)]
                s += ["  }}"]

        s += ["\nPseudovasp_potentials used:"]
        s += ["----------------------"]

        dates = []
        for symb, pot in self.get_potcars().items():
            with open(pot) as f:
                first_line = f.readline()
                date = first_line.strip().split()[2]
            s += [f"  {symb}: {pot.name} {date}"]
        return "\n".join(s).format(self=self)
