import os
import warnings
from pathlib import Path

from ase.io import read
import numpy as np

import storq.tools as tools
from storq.vasp.state_engine import StateEngine
import storq.vasp.validate as validate

# Mixins - order matters!
from storq.vasp.readers import Readers
import storq.vasp.readers as readers
from storq.vasp.getters import Getters
from storq.vasp.helpers import Helpers
from storq.vasp.relaxer import Relaxer
from storq.vasp.resource_manager import ResourceManager
from storq.vasp.runner import Runner
from storq.vasp.writers import Writers
from storq.vasp.serializers import Serializers
from storq.vasp.xc_defaults import xc_defaults


# To extend the VASP class we inherit from mixins classes.
class Vasp(
    Getters,
    Helpers,
    Readers,
    ResourceManager,
    Runner,
    Writers,
    Relaxer,
    Serializers,
):
    """Class for doing VASP calculations.

    This is the VASP calculator class. While it does not strictly adhere
    to the ASE Calculator interface (and hence does not inherit from it) it
    should still be compatible with all other ASE modules which use
    calculators.

    Parameters
    ----------
    directory : str
      The directory where the calculation files will be and
      the calculation run.

    atoms: Atoms
        The Atoms object on which the calculation will be performed.

    restart: bool
        Whether the calculator should be initialized using existing VASP files
        in the run directory.

    **kwargs
      Any VASP INCAR keyword can be used, e.g., ``encut=450``.
      Python types will be converted to a format appropriate for the INCAR
      file, e.g. True / False becomes  .TRUE. / .FALSE.

      Additionally, the following special keywords exist:

        xc : str
          exchange-correlation functional to use; it is expanded from
          :class:`Vasp.xc_defaults` to the relevant VASP tags.

        kpts : dict
            Controls how the KPOINTS file is written, admits the following key-value pairs:
               - size (``list``)
               - spacing (``float``)
               - auto (``float``)
               - list (``list``)
               - lines (``list``):
               - gamma (``bool``): if False use Monkhorst-Pack else Gamma-centered
               - shift (``list``): shift of the **k**-point grid
               - intersections: number of intersections used in line `vasp_mode`
               - cartesian (`bool`)

         setups : dict
          Special setups for the POTCARS. Items must be of the form {atom_symbol: suffix} e.g,  {'Zr': '_sv'}

        ldau_luj : dict
          This is a dictionary to set the DFT+U tags. For example, to put U=4
          on the d-orbitals (L=2) of Cu, and nothing on the oxygen atoms in a
          calculation use::

            ldau_luj={'Cu':{'L': 2, 'U':4.0, 'J':0.0},
                      'O': {'L':-1, 'U':0.0, 'J':0.0}},
    """

    version = "0.9"
    name = "VASP"

    # These allow you to use simple strings for the xc kwarg and automatically
    # set the relevant INCAR tags.
    xc_defaults = xc_defaults
    _default_parameters = {"lwave": False, "lcharg": False}

    # VASP Calculator states
    EMPTY = 0
    NEW = 1
    QUEUED = 2
    RUNNING = 3
    SYSTEM_CHANGED = 4
    FINISHED = 5
    UNFINISHED = 6
    EMPTY_CONTCAR = 7
    CONVERGED = 8
    UNCONVERGED = 9
    PARSED = 10
    UNKNOWN = 100

    # Read configuration of cluster resource manager
    siteconf = tools.read_siteconf()

    def __init__(self, directory, atoms=None, restart=True, **kwargs):
        self.directory = Path(directory).resolve()
        self.directory.mkdir(exist_ok=True)
        self.db = self.directory.joinpath("storq.json")
        self.name = "Vasp"
        self.atoms = None  # set later, needs special handling
        self.restart = restart
        self.parameters = {}
        self.state = Vasp.EMPTY
        self.jobid = None
        self.conf = {}
        self.atoms_on_file = {}
        self.results = {}

        self.xml = self.directory.joinpath("vasprun.xml")
        self.outcar = self.directory.joinpath("OUTCAR")
        self.incar = self.directory.joinpath("INCAR")
        self.kpoints = self.directory.joinpath("KPOINTS")
        self.potcar = self.directory.joinpath("POTCAR")
        self.poscar = self.directory.joinpath("POSCAR")

        # Restart option and configuration files, if no restart is requested
        # any previously existing files are nuked. Configs are then read in
        # the order storq.json -> ~/.config/storq
        if not self.restart:
            self.remove_files(files="all")
        if not self.db.is_file():
            self.conf.update(tools.read_batchconf())

        # How to set the calc paramters:
        #   1. Set default values.
        #   2. Update with values read from file.
        #   3. Update with kwargs passed to init.
        #

        # 1. Set defaults, if gga and pp were provided directly
        # dont use the default xc dict.
        self.set(**self._default_parameters)
        if {"gga", "pp"}.issubset(set(kwargs.keys())):
            kwargs.pop("xc", None)
        if "setups" in kwargs:
            self.parameters["setups"] = kwargs["setups"].copy()
            kwargs.pop("setups")

        # 2. Read atoms and paramters etc from existing files (results are read later)
        self.atoms_on_file = self.read_atoms()
        self.read_parameters()

        # 3. Now we can first set the atoms and then the parameters (some
        # of which depends on the atoms being set).
        if atoms is not None:
            self.set_atoms(atoms)
        else:
            self.set_atoms(self.atoms_on_file[0])
        self.set(**kwargs)

        # Optional: run parameter validation.
        if self.conf["vasp_validate"]:
            for key, val in self.parameters.items():
                if key in validate.__dict__:
                    if val is not None:
                        f = validate.__dict__[key]
                        f(self, val)
                else:
                    print("WARNING: no validation for {}".format(key))

    def set_atoms(self, atoms):
        """ Set the atoms attribute and attaches the calculator to it.
        """
        atoms._calc = self  # attach calc to atoms
        self.atoms = atoms
        self.isort = self.sort_atoms(atoms)

    def sort_atoms(self, atoms):
        """Determine the sorting used for file I/O.

        Parameters
        ----------
        atoms : Atoms
            configuration to sort
        """
        # Here we sort by element then use z-coord to tiebreak.
        symbs = atoms.get_chemical_symbols()
        setups = self.parameters.get("setups", None)
        if setups:
            # Strategy:
            # 1. Create unique list of symbols in the order we want them.
            # 2. Create list of tuples (symbol, z-coord) and then
            #    sort the indices of this list according to where the
            #    the symbol shows up shows up in the list from 1. and
            #    then use the z-coordinate to tiebreak.
            symbs_setups = list(setups.keys())
            symbs_uniq = symbs_setups + [
                s for s in set(symbs) if s not in symbs_setups
            ]
            symb_z = list(zip(symbs, atoms.positions[:, 2]))
            isort = np.array(
                sorted(
                    range(len(symb_z)),
                    key=lambda i: (symbs_uniq.index(symb_z[i][0]), symb_z[i][1])
                )
            )
        else:
            # If no setups were specified numpy takes care of the cake.
            isort = np.lexsort((atoms.positions[:, 2], symbs))
        return isort

    def set_xc_dict(self, val):
        """Set xc parameter.

        Adds all the xc_defaults flags for the chosen xc.

        """
        d = {"xc": val.lower()}
        oxc = self.parameters.get("xc", None)
        if oxc:
            for key in self.xc_defaults[oxc.lower()]:
                if key in self.parameters:
                    d[key] = None
        d.update(self.xc_defaults[val.lower()])
        self.parameters.update(d)

    def set(self, **kwargs):
        """Set parameters with keyword=value pairs.

        Parameters
        ----------
        **kwargs
            Can be any calculator keyword.
        """
        if "xc" in kwargs:
            self.set_xc_dict(kwargs["xc"])
            kwargs.pop("xc", None)

        if "ldau_luj" in kwargs:
            self.set_ldau_luj_dict(kwargs["ldau_luj"])
            kwargs.pop("ldau_luj", None)

        for key, val in kwargs.items():
            val_old = self.parameters.get(key, None)
            if key not in self.parameters or not tools.is_equal(val, val_old):
                self.parameters[key] = val

        # handle magmoms
        if self.parameters.get("ispin", 1) == 2:
            self.set_magmom()

    def set_magmom(self):
        """Handles magnetic moments.  """
        magmom = self.parameters.get("magmom", None)
        if magmom is not None:
            self.parameters["magmom"] = np.asarray(magmom)[self.isort]
        elif self.atoms.has("initial_magmoms"):
            imm_flat = self.atoms.arrays["initial_magmoms"][self.isort].flatten()
            self.parameters["magmom"] = imm_flat

    def set_rwigs_dict(self, val):
        """Return rwigs parameters."""
        d = {}
        if val is None:
            d["rwigs"] = None
            d["lorbit"] = None
        else:
            # val is a dictionary {symb: rwigs}
            # rwigs needs to be in the order of the potcars
            d["rwigs"] = [val[symb] for symb in self.get_potcars().keys()]
        self.parameters.update(d)

    def set_ldau_luj_dict(self, val):
        """Set the ldau_luj parameters."""
        # TODO investigate interaction with setups
        if val is not None:
            sorted_symbols = self.get_potcars().keys()
            d = {}
            d["ldaul"] = [val[symb]["L"] for symb in sorted_symbols]
            d["ldauu"] = [val[symb]["U"] for symb in sorted_symbols]
            d["ldauj"] = [val[symb]["J"] for symb in sorted_symbols]
        else:
            d = {}
            d["ldaul"] = None
            d["ldauu"] = None
            d["ldauj"] = None
        self.parameters.update(d)

    def has_changed(self):
        """Checks if there are any changes compared to last run.
        """
        # Compare atoms passed to recorded atoms
        mismatches = 0
        for atoms_file in self.atoms_on_file.values():
            isort_file = self.sort_atoms(atoms_file)
            system_changes = tools.compare_atoms(
                self.atoms[self.isort], atoms_file[isort_file]
            )
            if len(system_changes) != 0:
                mismatches += 1

        if 0 < mismatches == len(self.atoms_on_file):
            return True

        # Check for any parameter changes here.
        if len(self.parameters_on_file) == 0:
            return False

        for k, v in self.parameters.items():
            if k not in self.parameters_on_file and v is not None:
                return True
            elif k in self.parameters_on_file and not tools.is_equal(
                v, self.parameters_on_file[k]
            ):
                return True
        else:
            return False

    def calculation_required(self):
        """Checks whether a calculation is needed.

        Uses the internal state of the calculator to judge whether a
        calculation should be run. The result will be influenced by the
        calculator settings, e.g., whether strict convergence checks are
        enforced or not.
        TODO: SHOULD MAKE SENSE IF WE RUN THIS ON A COMPUTE NODE

        Returns
        -------
        bool
            True if calculation needs to be run
        """
        self.state = self.get_state()

        # if we have a brand new calculation or just
        # some input files we should run
        if self.state in [Vasp.NEW, Vasp.EMPTY]:
            return True

        # in we are in queue vasp_mode we check if we are waiting to run
        # or have an ongoing calculation
        if self.state == Vasp.QUEUED:
            print(
                "Batch job {1}: {0} is in the queue".format(
                    self.directory.name, self.jobid
                )
            )
            return False
        elif self.state == Vasp.RUNNING:
            print(
                "Batch job {1}: {0} is running".format(self.directory.name, self.jobid)
            )
            return False

        if self.state == Vasp.SYSTEM_CHANGED:
            return True

        # check if we have already parsed everything
        if self.state == Vasp.PARSED:
            return False

        # first we perform convergence checks if asked for
        if self.conf["vasp_convergence"] == "strict":
            if self.state == Vasp.CONVERGED:
                return False

            else:  # if the state is any other than converged
                if self.conf["vasp_restart_on"] == "convergence":
                    return True
                else:  # if we don't trigger a restarts just give a warning
                    warnings.warn(
                        "WARNING: {0}: Calculation is not converged.".format(
                            self.directory.name
                        )
                    )
                    return False
        elif self.conf["vasp_convergence"] == "basic":
            if self.state == Vasp.FINISHED:
                return False
            elif self.state == Vasp.UNFINISHED:
                if self.conf["vasp_restart_on"] == "basic":
                    return True
                else:  # if we do not care about convergence issue a warning
                    warnings.warn(
                        "WARNING: Vasp did not finish; directory: {}".format(
                            self.directory.name
                        )
                    )
                    return False
        else:  # convergence checks were None
            return False

    def get_state(self):
        """Determines calculation state based on directory contents.

        This method is only responsible for determining the state
        and does not act upon it. Other methods that cause a transition
        to a known state should alter the state themselves.
        TODO: SHOULD MAKE SENSE IF WE RUN THIS ON A COMPUTE NODE

        Returns
        -------
        int
            An integer corresponding to the internal
            state of the calculator.

        """
        # We do not check for KPOINTS here. That file may not exist if
        # the kspacing INCAR parameter is used.
        base_input = [f.is_file() for f in [self.incar, self.poscar, self.potcar]]
        if False in base_input:
            return Vasp.EMPTY

        # Input files exist, but no jobid, and no output
        if (
            np.array(base_input).all()
            and self.jobid is None
            and not self.outcar.is_file()
        ):
            return Vasp.NEW

        # INPUT files exist, a jobid in the queue
        job_info = self.get_job_info()
        if job_info is not None:
            status = job_info.get("st", None)
            if not status:  # certain versions of slurm use a different key
                status = job_info.get("state", None)
            if status == "PD":
                return Vasp.QUEUED
            elif status in ["R", "CG", "CD", "CF"]:
                return Vasp.RUNNING
            else:
                return Vasp.UNKNOWN

        # check if params or atoms have changed
        if self.has_changed():
            return Vasp.SYSTEM_CHANGED

        # check for convergence
        convergence_mode = self.conf["vasp_convergence"]
        if self.has_valid_outcar():
            if convergence_mode == "strict":
                if self.has_converged():
                    if self.results != {}:
                        return Vasp.PARSED
                    else:
                        return Vasp.CONVERGED
                else:
                    return Vasp.UNCONVERGED
            else:  # we just care about VASP exiting without errors
                if self.results != {}:
                    return Vasp.PARSED
                else:
                    return Vasp.FINISHED
        # if we get here there was an error or we hit batch_walltime
        else:
            return Vasp.UNFINISHED
        # check if contcar is empty
        if self.contcar.is_file():
            with open(self.contcar) as f:
                if f.read() == "":
                    return Vasp.EMPTY_CONTCAR

        # if we haven' returned yet the state is unknown
        return Vasp.UNKNOWN

    def get_atoms(self):
        """Gets a copy of the calculators Atoms object.

        Some ASE functions may rely on the calculator having this method.

        Returns
        -------
        Atoms
            copy of the configuration associated with the calculator
        """
        return self.atoms.copy()

    def get_property(self, name, atoms=None, allow_calculation=False):
        """Gets a property from the self.results dict.

        Some ASE functions may relay on the calculator having
        this method.

        Parameters
        ----------
        name : str
            name of property to get
        atoms : Atoms
            ignored but required by the ASE interface
        allow_calculation : bool
            ignored but required by the ASE interface

        Returns
        -------
        object
            The object representing the desired result.
        """
        if atoms is None:
            atoms = self.atoms

        if name == "magmom" and "magmom" not in self.results:
            return 0.0

        if name == "magmoms" and "magmoms" not in self.results:
            return np.zeros(len(atoms))

        if name not in self.results:
            return None
        else:
            result = self.results[name]
            if isinstance(result, np.ndarray):
                result = result.copy()
            return result

    # Patched ASE-mandated getters
    def get_potential_energy(self, atoms=None, force_consistent=False):
        """Gets the potential energy of the system.

        This method triggers a new calculation if necessary. Some ASE functions
        rely on the calculator having this method with this particular
        signature.

        Parameters
        ----------
        atoms : Atoms
            Only used if atoms.get_potential_energy() is called
        force_consistent : bool
            Toggle parsing of free energy.

        Returns
        -------
        float
            Energy or the free energy if force_consistent=True.

        """
        with StateEngine(self):
            energy = self.get_property("energy")
            if force_consistent:
                if "free_energy" not in self.results:
                    name = self.__class__.__name__
                    raise PropertyNotImplementedError(
                        'Force consistent/free energy ("free_energy") '
                        "not provided by {} calculator".format(name)
                    )
                return self.results["free_energy"]
            else:
                return energy

    def get_forces(self, atoms=None):
        """Gets the forces acting on the ions.

        This method triggers a new calculation if necessary.
        Some ASE functions rely on the calculator having
        this method with this particular signature.

        Parameters
        ----------
        atoms : Atoms
            Not used but required by ASE.

        Returns
        -------
        np.ndarray
            Array with forces acting on the ions.
        """
        with StateEngine(self):
            return self.get_property("forces", atoms)

    def get_magnetic_moment(self, atoms=None):
        """Get the magnetic moment.

        This method triggers a new calculation if necessary.
        Some ASE functions rely on the calculator having
        this method with this particular signature.

        Parameters
        ----------
        atoms : Atoms
            Not used but required by ASE.

        Returns
        -------
        np.ndarray
            Magnetic moment
        """
        with StateEngine(self):
            return self.get_property("magmom", atoms)

    def get_magnetic_moments(self, atoms=None):
        """Get the magnetic moments.

        This method triggers a new calculation if necessary.
        Some ASE functions rely on the calculator having
        this method with this particular signature.

        Parameters
        ----------
        atoms : Atoms
            Not used but required by ASE.

        Returns
        -------
        np.ndarray
            Magnetic moments
        """
        with StateEngine(self):
            return self.get_property("magmoms", atoms)

    def get_stress(self, atoms=None):
        """Gets the stress.

        This method triggers a new calculation if necessary.
        Some ASE functions rely on the calculator having
        this method with this particular signature.

        Parameters
        ----------
        atoms : Atoms
            Not used but required by ASE.

        Returns
        -------
        np.ndarray
            The stress.
        """

        with StateEngine(self):
            return self.get_property("stress", atoms)

    @property
    def traj(self):
        """ Read the calculation trajectory from ``vasprun.xml``.

        Returns
        -------
        list
            Atoms objects representing the trajectory

        """
        return read(self.xml, ":")

    def view(self, index=None):
        """Visualizes the calculation using the ASE gui.

        Parameters
        ---------
        index : int
            Index of a particular snapshot to view.

        Returns
        -------
        ASE gui view of the trajectory.

        """
        from ase.visualize import view

        if index is not None:
            return view(self.traj[index])
        else:
            return view(self.traj)

    def describe_parameters(self, long=False):
        """Describes the parameters using docstrings from vasp_validate."""
        for key in sorted(self.parameters.keys()):
            if key in validate.__dict__:
                f = validate.__dict__[key]
                d = f.__doc__ or "No description available."
                print("{} = {}:".format(key, self.parameters[key]))
                if long:
                    print("  " + d)
                else:
                    print("  " + d.split("\n")[0])
                print("")

    def run(self):
        """Convenience function for running the calculator."""
        return self.get_potential_energy()

    def configure(
        self,
        cores="ignore",
        nodes="ignore",
        walltime="ignore",
        account="ignore",
        options="ignore",
        prerun="ignore",
        mpi="ignore",
        **kwargs
    ):
        """Changes the configuration of the VASP calculator.

        Only allows changes to the calculator and not to the site configuration
        of the cluster resource manager. Note that any changes made
        to the configuration will be written to a database and persist until
        changed.

        Parameters
        ----------
        cores : int
            The number of cores to use in the calculation.
        nodes : int
            The number of nodes to use in the calculation.
        walltime : str
            The walltime of the calculation in the format '[d-]hh:mm:ss'.
        account : str
            The allocation on the supercomputing resource.
        options : str
            Space-separated string of extra options that will be passed
            to the resource manager.
        prerun : str
            Space-separated string of pre-run commands that will be passed
            to the resource manager.
        mpi : str
            Space-separated string of extra options that will be passed
            to the MPI executable or wrapper.
        **kwargs
            Any admissible storq settings.
        """
        shorthands = {
            "cores": cores,
            "nodes": nodes,
            "walltime": walltime,
            "account": account,
            "options": options,
            "prerun": prerun,
        }
        for short, val in shorthands.items():
            if val != "ignore":
                key = "batch_{}".format(short)
                kwargs[key] = val

        if mpi != "ignore":
            key = "mpi_options"
            kwargs[key] = mpi
        self.conf.update(kwargs)

    def has_converged(self):
        """Checks whether a calculation has converged.

        Performs a strict convergence checks in the sense that electronic
        as well as ionic convergence is explicitly checked for by comparing
        their magnitude to EDIFF and EDIFFG.

        Returns
        -------
        bool
            True if calculation is converged
        """
        converged = readers.read_convergence(self.outcar)

        # it only makes sense to check for ionic relaxation
        # if we used ibrion 1 or 2
        if (
            self.parameters.get("ibrion", None) in [1, 2]
            and self.parameters.get("nsw", 0) > 1
        ):
            return all(converged)
        else:
            return converged[0]

    def has_valid_outcar(self):
        """Checks if VASP finished properly.

        If VASP itself is canceled (batch_walltime or explicitly by user)
        or encounters certain errors it won't output the typical
        Voluntary context switch count on the last line.

        Returns
        -------
        bool
            True if VASP finished properly
        """
        return readers.read_outcar_validity(self.outcar)

    def ready(self):
        """Checks if the calculation is ready to be parsed.

        The outcome will be heavily dependent on the calculator
        configuration. For instance if the highest level of convergence
        check is enabled this will be enforced when checking for readiness.

        Returns
        -------
        bool
            True if the calculation is ready
        """
        if self.calculation_required() is True:
            return False
        elif self.state == Vasp.QUEUED:
            return False
        else:  # handle the special case of cell relaxations and restarting
            if self.parameters.get("isif", None) in range(3, 7):
                if self.get_num_ionic_steps() == 1:  # we're done
                    return True
                else:
                    # if we care about convergence we are not done
                    if self.conf.get("vasp_restart_on", None) == "convergence":
                        return False
                    else:  # we don't care and just warn the user
                        warnings.warn(
                            "WARNING: {}: Cell relaxation finished with more"
                            " than one ionic step".format(self.directory.name)
                        )
                        return True
            else:  # no cell relax so we're done
                return True

    def clone(self, new_dir):
        self.copy_files(new_dir)
        self.directory = Path(new_dir)
        self.write_persistence_db(self.atoms)

    def check_state(self, atoms, tol=1e-15):
        """ Required from interoperability with ASE DB
        """
        return tools.compare_atoms(self.atoms, atoms)
