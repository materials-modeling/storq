"""Stores different sets of VASP-related files that
are commonly operated on together.
"""
input_files = {"INCAR", "POSCAR", "KPOINTS", "POTCAR"}
output_files = {
        "OUTCAR",
        "OSZICAR",
        "vasprun.xml",
        "IBZKPT",
        "EIGENVAL",
        "CHG",
        "CHGCAR",
        "WAVECAR",
        "REPORT",
        "PCDAT",
        "CONTCAR",
        "XDATCAR",
        "LOCPOT",
        "ELFCAR",
        "DOSCAR",
        "PROCAR",
        "PROOUT",
        "storq.json",
        "vasp.out",
}

minimal_backup_base = {
    "INCAR", "POSCAR", "OUTCAR", "POTCAR", "OSZICAR", "vasprun.xml", "storq.json"
}
full_backup_base = input_files | output_files
vasp_files = full_backup_base - {"storq.json"}
vasp_files.add('STOPCAR')
