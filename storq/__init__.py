import os
from pathlib import Path
"""storq module"""

__project__ = 'storq'
__description__ = 'Every vasp needs a storq'
__authors__ = [u'Joakim Lofgren']
__copyright__ = '2019'
__license__ = 'GPL'
__credits__ = [u'Joakim Lofgren', 'John Kitchin']
__version__ = '0.9'
__maintainer__ = 'The storq developers team'
__email__ = 'storq@materialsmodeling.org'
__status__ = 'beta-version'
__url__ = 'http://storq.materialsmodeling.org/'


# here we make a convenience import of the Vasp class
# provided that storq has alreay been configured
if "STORQ_CONFIG_DIR" in os.environ:
    config_parent = Path(os.environ["STORQ_CONFIG_DIR"])
else:
    config_parent = Path('~/.config/storq').expanduser().resolve()
if config_parent.is_dir():
    config_site = config_parent / 'site.json'
    config_vasp = config_parent / 'vasp.json'
    if config_site.is_file() and config_vasp.is_file():
        from storq.vasp.core import Vasp
