## Storq

Take your VASP usage to the next level! No more collections of obscure bash
scripts, no illegible `sed`/`awk`-one liners, and no more wrestling with
[Slurm](https://en.wikipedia.org/wiki/Slurm_Workload_Manager) or
[TORQUE](https://en.wikipedia.org/wiki/TORQUE).
Keep it simple and pythonic. Keep it _storq_!

`storq`, which started out as a fork of [John Kitchin's alternative ASE VASP
calculator](https://github.com/jkitchin/vasp), consists of a submission system that communicates
with cluster resource managers such as
[Slurm](https://en.wikipedia.org/wiki/Slurm_Workload_Manager) and
[TORQUE](https://en.wikipedia.org/wiki/TORQUE) that is integrated with
an advanced ASE calculator for VASP. In the future, calculators for other popular _ab initio_
codes may be included.

The primary goal of `storq` is to provide work-flow optimizations
that greatly reduce the overhead of your day-to-day VASP calculations without introducing a graphical user interface.
In addition, many useful tools for high-throughput calculations are introduced.
The result is a modern, modular and hassle-free approach to VASP calculations.

Installation and usage instructions can be found in the [user guide](https://storq.materialsmodeling.org/).

# Quick setup
Start by cloning the git-repository

```bash
    cd ~/path/to/somewhere
    git clone git@gitlab.com:materials-modeling/storq.git
```

Now you can either proceed by installing via pip

```bash
    pip install --user -e storq
```

or by manually adding the paths to `storq` and its associated binaries to your ``.bashrc`` file

```bash
    export PYTHONPATH=$PYTHONPATH:~/path/to/somewhere/storq
    export PATH=$PATH:~/path/to/somewhere/storq/bin
```

Next you need to generate a configuration file. To this end, `storq` provides a command
line tool that handles the bulk of the configuration. To initiate the automatic configuration run

```bash
    storq configure --auto
```

The configuration process will alert you as to any settings that could not be detected automatically and hence need to be set manually. Note that some settings are optional, in which case only a warning is issued, while others are critical for the operation of `storq` and are hence marked as "fatal". To address any complaints for the automatic configuration,
 access the json configuration file through
```bash
    my_favourite_editor ~/.config/storq/vasp.json
```
or
```bash
    storq configure --f vasp
```
which will open the configuration file using `$EDITOR`. It might also be necessary to set the `mpi_command` field to match the supercomputing resource's preferred MPI run-command in order to be able to carry out calculations. This is the case if your supercomputing resource uses a wrapped version of the default `mpirun` command (e.g. `mpprun` on [NSC's triolith](https://www.nsc.liu.se/systems/triolith/) or`aprun` on [PDC's beskow](https://www.pdc.kth.se/hpc-services/computing-systems/beskow-1.737436)). It can furthermore be convenient to add a default allocation for your jobs to run on (you can override this later from your ASE script). To select an allocation, edit the value of the `batch_account` field to the name of your account (e.g., "snic20XX-X-XX" on a [SNIC](http://snic.se/) resource).
