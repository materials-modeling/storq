#!/usr/bin/env python3
import os
from storq.vasp.core import Vasp
import argparse

# we need to read the args used to call the relax_to_convergence
# function that are passed from the automated submission script
parser = argparse.ArgumentParser(
    description="Parse arguments passed via automatic vasp submission script."
)
parser.add_argument("cwd", type=str)
parser.add_argument("vaspdir", type=str)
parser.add_argument("max_runs", type=int)
parser.add_argument("--switch_opt", action="store_true", default=False)
parser.add_argument("--switch_tol_favg", type=float)
parser.add_argument("--switch_tol_fmax", type=float)
parser.add_argument("--backup", nargs="+")
args = parser.parse_args()

# handle the backup argument, we either get a list of file names
# or a single element list containing either minimal or full
if args.backup is not None:
    if len(args.backup) == 1 and args.backup[0] in ["minimal", "all"]:
        backup = args.backup[0]
    else:
        backup = args.backup
else:
    backup = None

# move back up to dir that contains calculator dir and
# restart calculator in run mode on the node
# os.chdir(args.cwd)

calc = Vasp(args.vaspdir)
calc.configure(mode="run")

# run the relaxation, we are now in run mode and will
# trigger a calculation and not a submission
calc._relax(
    max_runs=args.max_runs,
    backup=backup,
    switch_opt=args.switch_opt,
    switch_tol_favg=args.switch_tol_favg,
    switch_tol_fmax=args.switch_tol_fmax,
)
