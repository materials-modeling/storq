#!/usr/bin/env python3
import os
import argparse
from storq.vasp.core import Vasp

# we need to read the args used to call the relax_to_convergence
# function that are passed from the automated submission script
parser = argparse.ArgumentParser(
    description="Parse arguments passed via automatic vasp submission script."
)
parser.add_argument("cwd", type=str)
parser.add_argument("vaspdir", type=str)
args = parser.parse_args()

# move back up to dir that contains calculator dir and
# restart calculator in run mode on the node
# os.chdir(args.cwd)
calc = Vasp(args.vaspdir)
calc.configure(mode="run")

# calc restarted on a node has state queued so
# we need to change it here to run
# calc.state = Vasp.NEW

# run the relaxation, we are now in run mode and will
# trigger a calculation and not a submission
calc._run()
